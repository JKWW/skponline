﻿namespace Benchmark
{
    using System.Collections.Generic;
    using System.Linq;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Configs;
    using BenchmarkDotNet.Engines;
    using BenchmarkDotNet.Exporters;
    using BenchmarkDotNet.Exporters.Csv;
    using BenchmarkDotNet.Order;
    using SKPOnline;
    using SKPOnline.Database;
    using SKPOnline.Database.Models;
    using SKPOnline.Models;

    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class Benchmarks
    {
        private readonly Consumer consumer = new Consumer();

        public class Config : ManualConfig
        {
            public Config()
            {
                AddExporter(CsvMeasurementsExporter.Default);
                AddExporter(RPlotExporter.Default);
            }
        }

        [Benchmark]
        public List<Station> GetSmallData()
        {
            List<Station> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations.AsParallel().Take(10).ToList();
            }

            return result;
        }

        [Benchmark]
        public List<Station> GetBigData()
        {
            List<Station> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations.AsParallel().Take(1000).ToList();
            }

            return result;
        }

        [Benchmark]
        public List<Station> GetAllData()
        {
            List<Station> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations.AsParallel().ToList();
            }

            return result;
        }

        [Benchmark]
        public List<Station> GetByCondition()
        {
            List<Station> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations.AsParallel().Where(station => station.IdentificationCode.Contains("BR") && station.Name.Equals("Benchmark")).ToList();
            }

            return result;
        }

        [Benchmark]
        public List<Station> GetWithSorting()
        {
            List<Station> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations.AsParallel().OrderBy(station => station.IdentificationCode).ToList();
            }

            return result;
        }

        [Benchmark]
        public List<TestObject> GetWithMapping()
        {
            List<TestObject> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations.AsParallel().Select(station => new TestObject
                                                                           {
                                                                               Voivodeship = station.Voivodeship,
                                                                               City = station.City,
                                                                               Street = station.Street,
                                                                               HouseNumber = station.HouseNumber,
                                                                               ApartmentNumber = station.ApartmentNumber
                                                                           }).ToList();
            }

            return result;
        }

        [Benchmark]
        public List<IGrouping<string, Address>> ComplexGetter()
        {
            List<IGrouping<string, Address>> result;

            using (var dbContext = ApplicationDbContext.Create())
            {
                result = dbContext.Stations
                                  .AsParallel()
                                  .Where(station => station.IdentificationCode.Contains("BQ") && station.Name.Equals("Benchmark"))
                                  .Select(station => new Address
                                                     {
                                                         IdentificationCode = station.IdentificationCode,
                                                         Voivodeship = station.Voivodeship,
                                                         City = station.City,
                                                         Street = station.Street,
                                                         HouseNumber = station.HouseNumber,
                                                         ApartmentNumber = station.ApartmentNumber,
                                                     })
                                  .OrderBy(station => station.IdentificationCode)
                                  .ThenBy(station => station.Voivodeship)
                                  .GroupBy(station => station.IdentificationCode)
                                  .ToList();
            }

            return result;
        }

        public class TestObject
        {
            public string Voivodeship { get; set; }
            public string City { get; set; }
            public string Street { get; set; }
            public short HouseNumber { get; set; }
            public short? ApartmentNumber { get; set; }
        }
    }
}
