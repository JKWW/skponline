﻿namespace SKPOnline
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;
    using System.Web.WebPages;
    using Database;
    using Database.Models;
    using Models.ViewModels;

    public class EntityManager : IDisposable
    {
        public static EntityManager Create()
        {
            return new EntityManager();
        }

        public static Owner CreateOwnerDefaultProfile(string id)
        {
            return new Owner
                   {
                       Id = id,
                       Logo = GetDefaultPhoto(),
                       MyStations = new List<Station>()
                   };
        }

        public static Diagnostician CreateDiagnosticianDefaultProfile(string id)
        {
            return new Diagnostician
                   {
                       Id = id,
                       Avatar = GetDefaultPhoto(),
                       Workplaces = new List<Station>()
                   };
        }

        public static Customer CreateCustomerDefaultProfile(string id)
        {
            return new Customer
                   {
                       Id = id,
                       Avatar = GetDefaultPhoto(),
                       FavouriteStations = new List<Station>()
                   };
        }

        public static Station CreateDefaultStation()
        {
            return new Station
                   {
                       Diagnosticians = new List<Diagnostician>(),
                       Customers = new List<Customer>(),
                       Photo = GetDefaultPhoto()
                   };
        }

        public static Owner CreateOwnerProfile(string id,
                                               string companyName,
                                               string nip,
                                               string phoneNumber,
                                               string companyEmail,
                                               string voivodeship,
                                               string city,
                                               string street,
                                               short? houseNumber,
                                               short? apartmentNumber,
                                               List<Station> myStations,
                                               byte[] logo = null)
        {
            return new Owner
                   {
                       Id = id,
                       CompanyName = companyName,
                       Nip = nip,
                       PhoneNumber = phoneNumber,
                       CompanyEmail = companyEmail,
                       Voivodeship = voivodeship,
                       City = city,
                       Street = street,
                       HouseNumber = houseNumber,
                       ApartmentNumber = apartmentNumber,
                       Logo = logo ?? GetDefaultPhoto(),
                       MyStations = myStations
                   };
        }

        public static Diagnostician CreateDiagnosticianProfile(string id,
                                                               string permissionNumber,
                                                               string nickName,
                                                               string gender,
                                                               string firstName,
                                                               string surname,
                                                               DateTime birthDate,
                                                               string phoneNumber,
                                                               string voivodeship,
                                                               string city,
                                                               string street,
                                                               short? houseNumber,
                                                               short? apartmentNumber,
                                                               List<Station> workplaces,
                                                               byte[] avatar = null)
        {
            return new Diagnostician
                   {
                       Id = id,
                       PermissionNumber = permissionNumber,
                       NickName = nickName,
                       Gender = gender,
                       FirstName = firstName,
                       Surname = surname,
                       BirthDate = birthDate,
                       PhoneNumber = phoneNumber,
                       Voivodeship = voivodeship,
                       City = city,
                       Street = street,
                       HouseNumber = houseNumber,
                       ApartmentNumber = apartmentNumber,
                       Avatar = avatar ?? GetDefaultPhoto(),
                       Workplaces = workplaces,
                   };
        }

        public static Customer CreateCustomerProfile(string id,
                                                     string nickName,
                                                     string gender,
                                                     string firstName,
                                                     string surname,
                                                     DateTime birthDate,
                                                     string phoneNumber,
                                                     string voivodeship,
                                                     string city,
                                                     string street,
                                                     short? houseNumber,
                                                     short? apartmentNumber,
                                                     List<Station> favouriteStations,
                                                     byte[] avatar = null)
        {
            return new Customer
                   {
                       Id = id,
                       NickName = nickName,
                       Gender = gender,
                       FirstName = firstName,
                       Surname = surname,
                       BirthDate = birthDate,
                       PhoneNumber = phoneNumber,
                       Voivodeship = voivodeship,
                       City = city,
                       Street = street,
                       HouseNumber = houseNumber,
                       ApartmentNumber = apartmentNumber,
                       Avatar = avatar ?? GetDefaultPhoto(),
                       FavouriteStations = favouriteStations,
                   };
        }

        public static Station CreateStation(string name,
                                            string identificationCode,
                                            string voivodeship,
                                            string city,
                                            string street,
                                            short houseNumber,
                                            short? apartmentNumber,
                                            byte[] photo = null)
        {
            return new Station
                   {
                       Name = name,
                       IdentificationCode = identificationCode,
                       Voivodeship = voivodeship,
                       City = city,
                       Street = street,
                       HouseNumber = houseNumber,
                       ApartmentNumber = apartmentNumber,
                       Photo = photo,
                       Diagnosticians = new List<Diagnostician>(),
                       Customers = new List<Customer>()
                   };
        }

        public static void UpdateCustomerProfile(string id, CustomerProfileVM model)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var profile = dbContext.Customers.FirstOrDefault(customer => customer.Id == id);

                if (profile == null)
                {
                    return;
                }

                profile.NickName = model.NickName;
                profile.Gender = model.Gender;
                profile.FirstName = model.FirstName;
                profile.Surname = model.Surname;
                profile.BirthDate = model.BirthDate;
                profile.PhoneNumber = model.PhoneNumber;
                profile.Voivodeship = model.Voivodeship;
                profile.City = model.City;
                profile.Street = model.Street;
                profile.HouseNumber = model.HouseNumber;
                profile.ApartmentNumber = model.ApartmentNumber;
                profile.Avatar = model.Avatar;

                dbContext.SaveChanges();
            }
        }

        public static void UpdateDiagnosticianProfile(string id, DiagnosticianProfileVM model)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var profile = dbContext.Diagnosticians.FirstOrDefault(diagnostician => diagnostician.Id == id);

                if (profile == null)
                {
                    return;
                }

                profile.PermissionNumber = model.PermissionNumber;
                profile.NickName = model.NickName;
                profile.Gender = model.Gender;
                profile.FirstName = model.FirstName;
                profile.Surname = model.Surname;
                profile.BirthDate = model.BirthDate;
                profile.PhoneNumber = model.PhoneNumber;
                profile.Voivodeship = model.Voivodeship;
                profile.City = model.City;
                profile.Street = model.Street;
                profile.HouseNumber = model.HouseNumber;
                profile.ApartmentNumber = model.ApartmentNumber;
                profile.Avatar = model.Avatar;

                dbContext.SaveChanges();
            }
        }

        public static void UpdateOwnerProfile(string id, OwnerProfileVM model)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var profile = dbContext.Owners.FirstOrDefault(owner => owner.Id == id);

                if (profile == null)
                {
                    return;
                }

                profile.CompanyName = model.CompanyName;
                profile.Nip = model.Nip;
                profile.PhoneNumber = model.PhoneNumber;
                profile.CompanyEmail = model.CompanyEmail;
                profile.Voivodeship = model.Voivodeship;
                profile.City = model.City;
                profile.Street = model.Street;
                profile.HouseNumber = model.HouseNumber;
                profile.ApartmentNumber = model.ApartmentNumber;
                profile.Logo = model.Logo;

                dbContext.SaveChanges();
            }
        }

        public static void UpdateStationDetails(EditStationDetailsVM model)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var details = dbContext.Stations.FirstOrDefault(station => station.Id == model.Id);

                if (details == null)
                {
                    return;
                }

                details.Name = model.Name;
                details.IdentificationCode = model.IdentificationCode;
                details.Voivodeship = model.Voivodeship;
                details.City = model.City;
                details.Street = model.Street;
                details.HouseNumber = model.HouseNumber;
                details.ApartmentNumber = model.ApartmentNumber;
                details.Photo = model.Photo;

                dbContext.SaveChanges();
            }
        }

        public static bool IsNickNameUnique(string id, string nickName)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                return !dbContext.Customers.Any(customer => customer.NickName == nickName && customer.Id != id) &&
                       !dbContext.Diagnosticians.Any(diagnostician => diagnostician.NickName == nickName && diagnostician.Id != id);
            }
        }

        public static bool IsCompanyNameUnique(string id, string companyName)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                return !dbContext.Owners.Any(owner => owner.CompanyName == companyName && owner.Id != id);
            }
        }

        public static bool IsNipCorrect(string nip)
        {
            var digits = nip.Select(digit => (short) char.GetNumericValue(digit)).ToArray();
            var sum = digits[0] * 6 + digits[1] * 5 + digits[2] * 7 + digits[3] * 2 + digits[4] * 3 + digits[5] * 4 + digits[6] * 5 + digits[7] * 6 + digits[8] * 7;
            var checkDigit = sum % 11;

            return checkDigit == digits[9] && checkDigit != 10;
        }

        public static bool IsPermissionNumberUnique(string id, string permissionNumber)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                return !dbContext.Diagnosticians.Any(diagnostician => diagnostician.PermissionNumber == permissionNumber && diagnostician.Id != id);
            }
        }

        public static bool IsIdentificationCodeUnique(Guid id, string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                return !dbContext.Stations.Any(station => station.IdentificationCode == identificationCode && station.Id != id);
            }
        }

        public static bool IsIdentificationCodeUnique(string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                return !dbContext.Stations.Any(station => station.IdentificationCode == identificationCode);
            }
        }

        public static bool IsOwnerPublic(Owner owner)
        {
            if (owner == null)
            {
                return false;
            }

            return !owner.CompanyName.IsEmpty() && !owner.CompanyEmail.IsEmpty() && !owner.PhoneNumber.IsEmpty() && !owner.Nip.IsEmpty();
        }

        public static List<Diagnostician> GetPublicDiagnosticians(List<Diagnostician> diagnosticians)
        {
            return diagnosticians.Where(diagnostician => !diagnostician.PermissionNumber.IsEmpty() && !diagnostician.FirstName.IsEmpty() && !diagnostician.Surname.IsEmpty()).ToList();
        }

        public static bool IsItOwner(string id, string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var stationOwner = dbContext.Owners.FirstOrDefault(owner => owner.Id == id);

                return stationOwner != null && stationOwner.MyStations.Any(station => station.IdentificationCode == identificationCode);
            }
        }

        public static bool IsItFavourite(string id, string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var user = dbContext.Customers.FirstOrDefault(customer => customer.Id == id);

                return user != null && user.FavouriteStations.Any(station => station.IdentificationCode == identificationCode);
            }
        }

        public static bool IsItWorkplace(string id, string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var user = dbContext.Diagnosticians.FirstOrDefault(diagnostician => diagnostician.Id == id);

                return user != null && user.Workplaces.Any(station => station.IdentificationCode == identificationCode);
            }
        }

        public static bool IsStationRequestUseful(StationRequestVM model)
        {
            if (!model.Voivodeship.IsEmpty() && !model.City.IsEmpty() && !model.Street.IsEmpty() || !model.IdentificationCode.IsEmpty() || !model.KnownFrom.IsEmpty() || !model.OtherInfo.IsEmpty())
            {
                return true;
            }

            var filledFields = 0;

            if (!model.City.IsEmpty())
            {
                filledFields++;
            }

            if (!model.Name.IsEmpty())
            {
                filledFields++;
            }

            if (model.Photo != null)
            {
                filledFields++;
            }

            if (!model.Street.IsEmpty())
            {
                filledFields++;
            }

            if (!model.Voivodeship.IsEmpty())
            {
                filledFields++;
            }

            if (model.HouseNumber != null)
            {
                filledFields++;
            }

            if (!model.IdentificationCode.IsEmpty())
            {
                filledFields++;
            }

            if (!model.KnownFrom.IsEmpty())
            {
                filledFields++;
            }

            if (!model.OtherInfo.IsEmpty())
            {
                filledFields++;
            }

            return filledFields > 2;
        }

        public void Dispose()
        {
        }

        public static byte[] GetDefaultPhoto()
        {
            using (var memoryStream = new MemoryStream())
            {
                var image = Image.FromFile(HostingEnvironment.ApplicationPhysicalPath + @"Content\photo.png");

                image.Save(memoryStream, image.RawFormat);

                return memoryStream.ToArray();
            }
        }
    }
}
