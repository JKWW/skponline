﻿namespace SKPOnline
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Database;
    using Database.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;

    public static class DatabaseConfig
    {
        public static void CreateDefaultRoles()
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                using (var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbContext)))
                {
                    var roles = RolesEn.GetAllRoles();

                    foreach (var role in roles.Where(role => !roleManager.RoleExists(role)))
                    {
                        roleManager.Create(new IdentityRole(role));
                    }

                    dbContext.SaveChanges();
                }
            }
        }

        public static void CreateDefaultUsers()
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                using (var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbContext)))
                {
                    if (userManager.FindByEmail("admin@skponline.pl") == null)
                    {
                        var admin = new ApplicationUser
                                    {
                                        Email = "admin@skponline.pl",
                                        UserName = "admin@skponline.pl"
                                    };

                        userManager.Create(admin, "Admin1!");
                        userManager.AddToRole(admin.Id, RolesEn.Admin);
                    }

                    if (userManager.FindByEmail("wlasciciel@skponline.pl") == null)
                    {
                        var owner = new ApplicationUser
                                    {
                                        Email = "wlasciciel@skponline.pl",
                                        UserName = "wlasciciel@skponline.pl"
                                    };

                        userManager.Create(owner, "Wlasciciel1!");
                        userManager.AddToRole(owner.Id, RolesEn.Owner);
                    }

                    if (userManager.FindByEmail("diagnosta@skponline.pl") == null)
                    {
                        var diagnostician = new ApplicationUser
                                            {
                                                Email = "diagnosta@skponline.pl",
                                                UserName = "diagnosta@skponline.pl"
                                            };

                        userManager.Create(diagnostician, "Diagnosta1!");
                        userManager.AddToRole(diagnostician.Id, RolesEn.Diagnostician);
                    }

                    if (userManager.FindByEmail("klient@skponline.pl") == null)
                    {
                        var customer = new ApplicationUser
                                       {
                                           Email = "klient@skponline.pl",
                                           UserName = "klient@skponline.pl"
                                       };

                        userManager.Create(customer, "Klient1!");
                        userManager.AddToRole(customer.Id, RolesEn.Customer);
                    }

                    dbContext.SaveChanges();
                }
            }
        }

        public static void CreateDefaultProfiles()
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                using (var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbContext)))
                {
                    var defaultStationList = new List<Station>();

                    var owner = userManager.FindByEmail("wlasciciel@skponline.pl");

                    if (owner.Owner == null)
                    {
                        var ownerProfile = EntityManager.CreateOwnerProfile(owner.Id, "Testowa firma", "1234563218", "123456789", "testowa@firma.pl", VoivodeshipsPl.Mazowieckie, "Warszawa",
                                                                            "Marszałkowska", 15, null, defaultStationList);

                        dbContext.Owners.Add(ownerProfile);
                        dbContext.SaveChanges();
                    }

                    var diagnostician = userManager.FindByEmail("diagnosta@skponline.pl");

                    if (diagnostician.Diagnostician == null)
                    {
                        var diagnosticianProfile = EntityManager.CreateDiagnosticianProfile(diagnostician.Id, "XXX/D/0000", "Fajny Diagnosta", GenderPl.Mężczyzna, "Test", "Testowy", DateTime.Today,
                                                                                            "123465789", VoivodeshipsPl.Lubelskie, "Lublin", "Nowa", 9, 1, defaultStationList);

                        dbContext.Diagnosticians.Add(diagnosticianProfile);
                        dbContext.SaveChanges();
                    }

                    var customer = userManager.FindByEmail("klient@skponline.pl");

                    if (customer.Customer == null)
                    {
                        var customerProfile = EntityManager.CreateCustomerProfile(customer.Id, "Fajna Klientka", GenderPl.Kobieta, "Test", "Testowa", DateTime.Today, "123456789",
                                                                                  VoivodeshipsPl.Mazowieckie, "Warszawa", "Krakowskie Przedmieście", 13, 3, defaultStationList);

                        dbContext.Customers.Add(customerProfile);
                        dbContext.SaveChanges();
                    }

                    dbContext.SaveChanges();
                }
            }
        }
    }
}
