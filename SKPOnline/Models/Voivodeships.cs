﻿namespace SKPOnline.Models
{
    using System.Collections.Generic;

    public static class VoivodeshipsEn
    {
        public const string LowerSilesia = "Lower Silesia";
        public const string KuyaviaPomerania = "Kuyavia-Pomerania";
        public const string LodzProvince = "Lodz Province";
        public const string LublinProvince = "Lublin Province";
        public const string LubuszProvince = "Lubusz Province";
        public const string LesserPoland = "Lesser Poland";
        public const string Masovia = "Masovia";
        public const string Subcarpathia = "Subcarpathia";
        public const string Pomerania = "Pomerania";
        public const string Silesia = "Silesia";
        public const string WarmiaMasuria = "Warmia-Masuria";
        public const string GreaterPoland = "Greater Poland";
        public const string WestPomerania = "West Pomerania";
        public const string HolyCross = "Holy Cross";
        public const string PodlasieProvince = "Podlasie Province";
        public const string OpoleProvince = "Opole Province";

        public static IEnumerable<string> GetAllVoivodeships()
        {
            var voivodeships = new List<string>
                               {
                                   LowerSilesia,
                                   KuyaviaPomerania,
                                   LodzProvince,
                                   LublinProvince,
                                   LubuszProvince,
                                   LesserPoland,
                                   Masovia,
                                   Subcarpathia,
                                   Pomerania,
                                   Silesia,
                                   WarmiaMasuria,
                                   GreaterPoland,
                                   WestPomerania,
                                   HolyCross,
                                   PodlasieProvince,
                                   OpoleProvince
                               };

            voivodeships.Sort();

            return voivodeships;
        }
    }

    public static class VoivodeshipsPl
    {
        public const string Dolnośląskie = "Dolnośląskie";
        public const string KujawskoPomorskie = "Kujawsko Pomorskie";
        public const string Łódzkie = "Łódzkie";
        public const string Lubelskie = "Lubelskie";
        public const string Lubuskie = "Lubuskie";
        public const string Małopolskie = "Małopolskie";
        public const string Mazowieckie = "Mazowieckie";
        public const string Podkarpackie = "Podkarpackie";
        public const string Pomorskie = "Pomorskie";
        public const string Śląskie = "Śląskie";
        public const string WarmińskoMazurskie = "Warmińsko-Mazurskie";
        public const string Wielkopolskie = "Wielkopolskie";
        public const string Zachodniopomorskie = "Zachodnio-pomorskie";
        public const string Świętokrzyskie = "Świętokrzyskie";
        public const string Podlaskie = "Podlaskie";
        public const string Opolskie = "Opolskie";

        public static IEnumerable<string> GetAllVoivodeships()
        {
            var voivodeships = new List<string>
                               {
                                   Dolnośląskie,
                                   KujawskoPomorskie,
                                   Łódzkie,
                                   Lubelskie,
                                   Lubuskie,
                                   Małopolskie,
                                   Mazowieckie,
                                   Podkarpackie,
                                   Pomorskie,
                                   Śląskie,
                                   WarmińskoMazurskie,
                                   Wielkopolskie,
                                   Zachodniopomorskie,
                                   Świętokrzyskie,
                                   Podlaskie,
                                   Opolskie
                               };

            voivodeships.Sort();

            return voivodeships;
        }
    }
}
