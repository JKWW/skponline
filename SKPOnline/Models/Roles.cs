﻿namespace SKPOnline.Models
{
    using System.Collections.Generic;

    public static class RolesEn
    {
        public const string Admin = "Admin";
        public const string Owner = "Owner";
        public const string Diagnostician = "Diagnostician";
        public const string Customer = "Customer";

        public static IEnumerable<string> GetAllRoles()
        {
            var roles = new List<string>
                        {
                            Admin,
                            Owner,
                            Diagnostician,
                            Customer
                        };

            roles.Sort();

            return roles;
        }

        public static IEnumerable<string> GetUserRoles()
        {
            var roles = new List<string>
                        {
                            Owner,
                            Diagnostician,
                            Customer
                        };

            roles.Sort();

            return roles;
        }
    }

    public static class RolesPl
    {
        public const string Admin = "Admin";
        public const string Właściciel = "Właściciel";
        public const string Diagnosta = "Diagnosta";
        public const string Klient = "Klient";

        public static IEnumerable<string> GetAllRoles()
        {
            var roles = new List<string>
                        {
                            Admin,
                            Właściciel,
                            Diagnosta,
                            Klient
                        };

            roles.Sort();

            return roles;
        }

        public static IEnumerable<string> GetUserRoles()
        {
            var roles = new List<string>
                        {
                            Właściciel,
                            Diagnosta,
                            Klient
                        };

            roles.Sort();

            return roles;
        }
    }
}
