﻿namespace SKPOnline.Models
{
    using System.Collections.Generic;

    public static class GenderEn
    {
        public const string Male = "Male";
        public const string Female = "Female";
        public const string Other = "Other";

        public static IEnumerable<string> GetAllGenders()
        {
            var genders = new List<string>
                          {
                              Male,
                              Female,
                              Other
                          };

            genders.Sort();

            return genders;
        }
    }

    public static class GenderPl
    {
        public const string Mężczyzna = "Mężczyzna";
        public const string Kobieta = "Kobieta";
        public const string Inna = "Inna";

        public static IEnumerable<string> GetAllGenders()
        {
            var genders = new List<string>
                          {
                              Mężczyzna,
                              Kobieta,
                              Inna
                          };

            genders.Sort();

            return genders;
        }
    }
}
