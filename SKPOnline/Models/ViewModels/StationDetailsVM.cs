﻿namespace SKPOnline.Models.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Database.Models;

    public class StationDetailsVM
    {
        [Display(Name = "Nazwa własna SKP")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Nazwa własna SKP może mieć od {2} do {1}")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Kod rozpoznawczy SKP jest wymagany")]
        [Display(Name = "Kod rozpoznawczy SKP")]
        [StringLength(9, MinimumLength = 6, ErrorMessage = "Kod rozpoznawczy stacji może mieć od {2} do {1} znaków.")]
        [RegularExpression(@"^[A-Z]{2,3}\/[0-9]{3}(\/[P])?$", ErrorMessage = "Kod rozpoznawczy stacji jest nieprawidłowy.")]
        public string IdentificationCode { get; set; }

        [Required(ErrorMessage = "Województwo, w którym znajduje się SKP jest wymagana!")]
        [Display(Name = "Województwo")]
        public string Voivodeship { get; set; }

        [Required(ErrorMessage = "Miejscowość, w której znajduje się SKP jest wymagana!")]
        [Display(Name = "Miejscowość")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości może mieć od {2} do {1} znaków.")]
        public string City { get; set; }

        [Required(ErrorMessage = "Ulica, na której znajduje się SKP jest wymagana!")]
        [Display(Name = "Ulica")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Nazwa ulicy może mieć od {2} do {1} znaków.")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Numer posesji, na której znajduje się SKP jest wymagane!")]
        [Display(Name = "Numer posesji")]
        [Range(1, 1000, ErrorMessage = "Numer posesji może być w przedziale od {1} do {2}")]
        public short HouseNumber { get; set; }

        [Display(Name = "Numer lokalu")]
        [Range(1, 1000, ErrorMessage = "Numer lokalu może być w przedziale od {1} do {2}")]
        public short? ApartmentNumber { get; set; }

        [Display(Name = "Zdjęcie przedstawiające SKP")]
        public byte[] Photo { get; set; }

        [Display(Name = "Diagności")]
        public List<Diagnostician> Diagnosticians { get; set; }

        [Display(Name = "Właściciel stacji kontroli pojazdów")]
        public Owner Owner { get; set; }
    }
}
