﻿namespace SKPOnline.Models.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class StationRequestVM
    {
        [Required(ErrorMessage = "Id wniosku jest wymagane!")]
        public Guid Id { get; set; }

        [Display(Name = "Skąd wiesz o tej SKP")]
        [StringLength(200, ErrorMessage = "Uzasadnienie może mieć maksymalnie {1} znaków")]
        public string KnownFrom { get; set; }

        [Display(Name = "Nazwa własna SKP")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Nazwa własna SKP może mieć od {2} do {1}")]
        public string Name { get; set; }

        [Display(Name = "Kod rozpoznawczy SKP")]
        [StringLength(9, MinimumLength = 6, ErrorMessage = "Kod rozpoznawczy stacji może mieć od {2} do {1} znaków.")]
        [RegularExpression(@"^[A-Z]{2,3}\/[0-9]{3}(\/[P])?$", ErrorMessage = "Kod rozpoznawczy stacji jest nieprawidłowy.")]
        public string IdentificationCode { get; set; }

        [Display(Name = "Województwo")]
        public string Voivodeship { get; set; }

        [Display(Name = "Miejscowość")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości może mieć od {2} do {1} znaków.")]
        public string City { get; set; }

        [Display(Name = "Ulica")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Nazwa ulicy może mieć od {2} do {1} znaków.")]
        public string Street { get; set; }

        [Display(Name = "Numer posesji")]
        [Range(1, 1000, ErrorMessage = "Numer posesji może być w przedziale od {1} do {2}")]
        public short? HouseNumber { get; set; }

        [Display(Name = "Numer lokalu")]
        [Range(1, 1000, ErrorMessage = "Numer lokalu może być w przedziale od {1} do {2}")]
        public short? ApartmentNumber { get; set; }

        [Display(Name = "Pozostałe informacje")]
        [StringLength(1000, ErrorMessage = "Pozostałe informacje mogą mieć maksymalnie {1} znaków")]
        public string OtherInfo { get; set; }

        [Display(Name = "Zdjęcie przedstawiające SKP")]
        public byte[] Photo { get; set; }
    }
}
