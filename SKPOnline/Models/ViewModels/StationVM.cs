﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class StationVM
    {
        [Required(ErrorMessage = "Kod rozpoznawczy SKP jest wymagany")]
        [Display(Name = "Kod rozpoznawczy SKP")]
        [StringLength(9, MinimumLength = 6, ErrorMessage = "Kod rozpoznawczy stacji może mieć od {2} do {1} znaków.")]
        [RegularExpression(@"^[A-Z]{2,3}\/[0-9]{3}(\/[P])?$", ErrorMessage = "Kod rozpoznawczy stacji jest nieprawidłowy.")]
        public string IdentificationCode { get; set; }

        [Required(ErrorMessage = "Województwo, w którym znajduje się SKP jest wymagana!")]
        [Display(Name = "Województwo")]
        public string Voivodeship { get; set; }

        [Required(ErrorMessage = "Miejscowość, w której znajduje się SKP jest wymagana!")]
        [Display(Name = "Miejscowość")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości może mieć od {2} do {1} znaków.")]
        public string City { get; set; }
    }
}
