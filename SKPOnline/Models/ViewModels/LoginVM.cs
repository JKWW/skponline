﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class LoginVM
    {
        [Required(ErrorMessage = "Pole Adres e-mail jest wymagane.")]
        [Display(Name = "Adres e-mail")]
        [EmailAddress(ErrorMessage = "Wprowadzony adres e-mail ma niepoprawny format.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [DataType(DataType.Password, ErrorMessage = "Niewłaściwy typ danych.")]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Nie wylogowywuj mnie?")]
        public bool RememberMe { get; set; }
    }
}
