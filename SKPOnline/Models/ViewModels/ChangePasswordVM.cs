﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class ChangePasswordVM
    {
        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [DataType(DataType.Password, ErrorMessage = "Niewłaściwy typ danych.")]
        [Display(Name = "Bieżące hasło")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [StringLength(100, ErrorMessage = "{0} musi mieć co najmniej następującą liczbę znaków: {2}.", MinimumLength = 6)]
        [DataType(DataType.Password, ErrorMessage = "Niewłaściwy typ danych.")]
        [Display(Name = "Nowe hasło")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [DataType(DataType.Password, ErrorMessage = "Niewłaściwy typ danych.")]
        [Display(Name = "Potwierdź nowe hasło")]
        [Compare("NewPassword", ErrorMessage = "Hasła nie są zgodne.")]
        public string ConfirmPassword { get; set; }
    }
}
