﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class ChangeEmailVM
    {
        [Required(ErrorMessage = "Pole Adres e-mail jest wymagane.")]
        [EmailAddress(ErrorMessage = "Wprowadzony adres e-mail ma niepoprawny format.")]
        [Display(Name = "Adres e-mail")]
        public string NewEmail { get; set; }

        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [StringLength(100, ErrorMessage = "{0} musi zawierać co najmniej następującą liczbę znaków: {2}.", MinimumLength = 6)]
        [DataType(DataType.Password, ErrorMessage = "Niewłaściwy typ danych.")]
        [Display(Name = "Hasło")]
        public string Password { get; set; }
    }
}
