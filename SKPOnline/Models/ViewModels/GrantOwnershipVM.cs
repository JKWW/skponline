﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class GrantOwnershipVM
    {
        [Required(ErrorMessage = "Kod rozpoznawczy SKP jest wymagany")]
        [Display(Name = "Kod rozpoznawczy SKP")]
        [StringLength(9, MinimumLength = 6, ErrorMessage = "Kod rozpoznawczy stacji może mieć od {2} do {1} znaków.")]
        [RegularExpression(@"^[A-Z]{2,3}\/[0-9]{3}(\/[P])?$", ErrorMessage = "Kod rozpoznawczy stacji jest nieprawidłowy.")]
        public string IdentificationCode { get; set; }

        [Required(ErrorMessage = "Id profilu właściciela jest wymagane!")]
        [Display(Name = "Id profilu właściciela")]
        public string OwnerId { get; set; }
    }
}
