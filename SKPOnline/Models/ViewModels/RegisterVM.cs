﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class RegisterVM
    {
        [Required(ErrorMessage = "Określenie typu konta jest wymagane.")]
        [Display(Name = "Typ konta")]
        public string Role { get; set; }

        [Required(ErrorMessage = "Pole Adres e-mail jest wymagane.")]
        [EmailAddress(ErrorMessage = "Wprowadzony adres e-mail ma niepoprawny format.")]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [StringLength(100, ErrorMessage = "{0} musi zawierać co najmniej następującą liczbę znaków: {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Pole Hasło jest wymagane.")]
        [DataType(DataType.Password, ErrorMessage = "Niewłaściwy typ danych.")]
        [Display(Name = "Potwierdź hasło")]
        [Compare("Password", ErrorMessage = "Hasła nie są zgodne.")]
        public string ConfirmPassword { get; set; }
    }
}
