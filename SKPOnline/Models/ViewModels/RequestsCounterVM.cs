﻿namespace SKPOnline.Models.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class RequestsCounterVM
    {
        [Required(ErrorMessage = "Podanie ilości wniosków jest wymagane.")]
        public int NumberOfStationRequests { get; set; }

        [Required(ErrorMessage = "Podanie ilości wniosków jest wymagane.")]
        public int NumberOfOnwerStationRequests { get; set; }

        [Required(ErrorMessage = "Podanie ilości wniosków jest wymagane.")]
        public int NumberOfOwnershipRequests { get; set; }
    }
}
