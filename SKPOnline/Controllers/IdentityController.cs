﻿namespace SKPOnline.Controllers
{
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Database;
    using Database.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Models;
    using Models.ViewModels;

    [Authorize]
    public class IdentityController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public IdentityController()
        {
        }

        public IdentityController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        private ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            set => _signInManager = value;
        }

        private ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            set => _userManager = value;
        }

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole(RolesEn.Admin))
                {
                    return RedirectToAction("Index", "Admin", new {message = "Jesteś już zalogowany!"});
                }

                if (User.IsInRole(RolesEn.Customer))
                {
                    return RedirectToAction("Index", "Customer", new {message = "Jesteś już zalogowany!"});
                }

                if (User.IsInRole(RolesEn.Diagnostician))
                {
                    return RedirectToAction("Index", "Diagnostician", new {message = "Jesteś już zalogowany!"});
                }

                if (User.IsInRole(RolesEn.Owner))
                {
                    return RedirectToAction("Index", "Owner", new {message = "Jesteś już zalogowany!"});
                }
            }
            
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginVM model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole(RolesEn.Admin))
                {
                    return RedirectToAction("Index", "Admin", new {message = "Jesteś już zalogowany!"});
                }

                if (User.IsInRole(RolesEn.Customer))
                {
                    return RedirectToAction("Index", "Customer", new {message = "Jesteś już zalogowany!"});
                }

                if (User.IsInRole(RolesEn.Diagnostician))
                {
                    return RedirectToAction("Index", "Diagnostician", new {message = "Jesteś już zalogowany!"});
                }

                if (User.IsInRole(RolesEn.Owner))
                {
                    return RedirectToAction("Index", "Owner", new {message = "Jesteś już zalogowany!"});
                }
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

            switch (result)
            {
                case SignInStatus.Success:
                {
                    return RedirectToLocal(returnUrl);
                }
                default:
                {
                    ModelState.AddModelError("", @"Nieprawidłowa próba logowania.");
                    return View(model);
                }
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }

            if (User.IsInRole(RolesEn.Admin))
            {
                return RedirectToAction("Index", "Admin", new {message = "Posiadasz już konto!"});
            }

            if (User.IsInRole(RolesEn.Customer))
            {
                return RedirectToAction("Index", "Customer", new {message = "Posiadasz już konto!"});
            }

            if (User.IsInRole(RolesEn.Diagnostician))
            {
                return RedirectToAction("Index", "Diagnostician", new {message = "Posiadasz już konto!"});
            }

            if (User.IsInRole(RolesEn.Owner))
            {
                return RedirectToAction("Index", "Owner", new {message = "Posiadasz już konto!"});
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterVM model)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole(RolesEn.Admin))
                {
                    return RedirectToAction("Index", "Admin", new {message = "Posiadasz już konto!"});
                }

                if (User.IsInRole(RolesEn.Customer))
                {
                    return RedirectToAction("Index", "Customer", new {message = "Posiadasz już konto!"});
                }

                if (User.IsInRole(RolesEn.Diagnostician))
                {
                    return RedirectToAction("Index", "Diagnostician", new {message = "Posiadasz już konto!"});
                }

                if (User.IsInRole(RolesEn.Owner))
                {
                    return RedirectToAction("Index", "Owner", new {message = "Posiadasz już konto!"});
                }
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = new ApplicationUser
                       {
                           UserName = model.Email,
                           Email = model.Email
                       };
            var result = await UserManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                switch (model.Role)
                {
                    case RolesPl.Klient:
                    {
                        UserManager.AddToRole(user.Id, RolesEn.Customer);

                        using (var dbContext = ApplicationDbContext.Create())
                        {
                            dbContext.Customers.Add(EntityManager.CreateCustomerDefaultProfile(user.Id));
                            await dbContext.SaveChangesAsync();
                        }

                        break;
                    }
                    case RolesPl.Diagnosta:
                    {
                        UserManager.AddToRole(user.Id, RolesEn.Diagnostician);

                        using (var dbContext = ApplicationDbContext.Create())
                        {
                            dbContext.Diagnosticians.Add(EntityManager.CreateDiagnosticianDefaultProfile(user.Id));
                            await dbContext.SaveChangesAsync();
                        }

                        break;
                    }
                    case RolesPl.Właściciel:
                    {
                        UserManager.AddToRole(user.Id, RolesEn.Owner);

                        using (var dbContext = ApplicationDbContext.Create())
                        {
                            dbContext.Owners.Add(EntityManager.CreateOwnerDefaultProfile(user.Id));
                            await dbContext.SaveChangesAsync();
                        }

                        break;
                    }
                }

                await SignInManager.SignInAsync(user, false, false);

                return RedirectToAction("Index", "Home");
            }

            AddErrors(result);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (!error.Contains("Nazwa"))
                {
                    ModelState.AddModelError("", error);
                }
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
