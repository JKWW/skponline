﻿namespace SKPOnline.Controllers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Database;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.ViewModels;

    [Authorize(Roles = RolesEn.Owner)]
    public class OwnerController : Controller
    {
        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            OwnerProfileVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var profile = dbContext.Owners.FirstOrDefault(customer => customer.Id == userId);
                model = new OwnerProfileVM
                        {
                            CompanyName = profile?.CompanyName,
                            Nip = profile?.Nip,
                            PhoneNumber = profile?.PhoneNumber,
                            CompanyEmail = profile?.CompanyEmail,
                            Voivodeship = profile?.Voivodeship,
                            City = profile?.City,
                            Street = profile?.Street,
                            HouseNumber = profile?.HouseNumber,
                            ApartmentNumber = profile?.ApartmentNumber,
                            Logo = profile?.Logo
                        };
            }

            return View(model);
        }

        public ActionResult Edit()
        {
            OwnerProfileVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var profile = dbContext.Owners.FirstOrDefault(customer => customer.Id == userId);
                model = new OwnerProfileVM
                        {
                            CompanyName = profile?.CompanyName,
                            Nip = profile?.Nip,
                            PhoneNumber = profile?.PhoneNumber,
                            CompanyEmail = profile?.CompanyEmail,
                            Voivodeship = profile?.Voivodeship,
                            City = profile?.City,
                            Street = profile?.Street,
                            HouseNumber = profile?.HouseNumber,
                            ApartmentNumber = profile?.ApartmentNumber,
                            Logo = profile?.Logo
                        };
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OwnerProfileVM model, HttpPostedFileBase image = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userId = User.Identity.GetUserId();

            if (!EntityManager.IsCompanyNameUnique(userId, model.CompanyName))
            {
                ModelState.AddModelError(nameof(model.CompanyName), "Taka nazwa firmy już istnieje.");
                return View(model);
            }

            if (!EntityManager.IsNipCorrect(model.Nip))
            {
                ModelState.AddModelError(nameof(model.Nip), "Numer NIP jest nieprawidłowy.");
                return View(model);
            }

            if (image != null)
            {
                var fileStream = image.InputStream;
                var binaryReader = new BinaryReader(fileStream);
                model.Logo = binaryReader.ReadBytes((int) fileStream.Length);
            }

            EntityManager.UpdateOwnerProfile(userId, model);

            return RedirectToAction("Index", new {message = "Poprawnie zaktualizowano profil!"});
        }

        public ActionResult Stations(string message)
        {
            ViewBag.StatusMessage = message;

            List<StationVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Owners.FirstOrDefault(owner => owner.Id == userId);

                if (user == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Wystąpił błąd podczas pobierania listy Twoich SKP!"});
                }

                model = user.MyStations
                            .Select(station => new StationVM
                                                          {
                                                              IdentificationCode = station.IdentificationCode,
                                                              Voivodeship = station.Voivodeship,
                                                              City = station.City
                                                          })
                            .OrderBy(station => station.IdentificationCode)
                            .ToList();
            }

            return View(model);
        }
    }
}
