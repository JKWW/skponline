﻿namespace SKPOnline.Controllers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Database;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.ViewModels;

    [Authorize(Roles = RolesEn.Diagnostician)]
    public class DiagnosticianController : Controller
    {
        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            DiagnosticianProfileVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var profile = dbContext.Diagnosticians.FirstOrDefault(customer => customer.Id == userId);
                model = new DiagnosticianProfileVM
                        {
                            PermissionNumber = profile?.PermissionNumber,
                            NickName = profile?.NickName,
                            Gender = profile?.Gender,
                            FirstName = profile?.FirstName,
                            Surname = profile?.Surname,
                            BirthDate = profile?.BirthDate,
                            PhoneNumber = profile?.PhoneNumber,
                            Voivodeship = profile?.Voivodeship,
                            City = profile?.City,
                            Street = profile?.Street,
                            HouseNumber = profile?.HouseNumber,
                            ApartmentNumber = profile?.ApartmentNumber,
                            Avatar = profile?.Avatar
                        };
            }

            return View(model);
        }

        public ActionResult Edit()
        {
            DiagnosticianProfileVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var profile = dbContext.Diagnosticians.FirstOrDefault(customer => customer.Id == userId);
                model = new DiagnosticianProfileVM
                        {
                            PermissionNumber = profile?.PermissionNumber,
                            NickName = profile?.NickName,
                            Gender = profile?.Gender,
                            FirstName = profile?.FirstName,
                            Surname = profile?.Surname,
                            BirthDate = profile?.BirthDate,
                            PhoneNumber = profile?.PhoneNumber,
                            Voivodeship = profile?.Voivodeship,
                            City = profile?.City,
                            Street = profile?.Street,
                            HouseNumber = profile?.HouseNumber,
                            ApartmentNumber = profile?.ApartmentNumber,
                            Avatar = profile?.Avatar
                        };
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DiagnosticianProfileVM model, HttpPostedFileBase image = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userId = User.Identity.GetUserId();

            if (!EntityManager.IsNickNameUnique(userId, model.NickName))
            {
                ModelState.AddModelError(nameof(model.NickName), "Taka nazwa użytkownika już istnieje.");
                return View(model);
            }

            if (!EntityManager.IsPermissionNumberUnique(userId, model.PermissionNumber))
            {
                ModelState.AddModelError(nameof(model.PermissionNumber), "Użytkownik z takim numerem uprawnień już istnieje.");
                return View(model);
            }

            if (image != null)
            {
                var fileStream = image.InputStream;
                var binaryReader = new BinaryReader(fileStream);
                model.Avatar = binaryReader.ReadBytes((int) fileStream.Length);
            }

            EntityManager.UpdateDiagnosticianProfile(userId, model);

            return RedirectToAction("Index", new {message = "Poprawnie zaktualizowano profil!"});
        }

        public ActionResult Stations(string message)
        {
            ViewBag.StatusMessage = message;

            List<StationVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Diagnosticians.FirstOrDefault(diagnostician => diagnostician.Id == userId);

                if (user == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Wystąpił błąd podczas pobierania listy Twoich miejsc pracy!"});
                }

                model = user.Workplaces
                            .Select(station => new StationVM
                                               {
                                                   IdentificationCode = station.IdentificationCode,
                                                   Voivodeship = station.Voivodeship,
                                                   City = station.City
                                               })
                            .OrderBy(station => station.IdentificationCode)
                            .ToList();
            }

            return View(model);
        }
    }
}
