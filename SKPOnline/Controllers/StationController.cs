﻿namespace SKPOnline.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Database;
    using Database.Models;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.ViewModels;

    public class StationController : Controller
    {
        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            List<StationVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.Stations
                                 .Select(station => new StationVM
                                                    {
                                                        IdentificationCode = station.IdentificationCode,
                                                        Voivodeship = station.Voivodeship,
                                                        City = station.City
                                                    })
                                 .OrderBy(station => station.Voivodeship)
                                 .ToList();
            }

            return View(model);
        }

        public ActionResult Details(string message, string identificationCode)
        {
            ViewBag.StatusMessage = message;

            StationDetailsVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var chosenStation = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (chosenStation == null)
                {
                    return RedirectToAction("Index", new {message = "Wystąpił błąd podczas wyświetlania strony wybranej SKP."});
                }

                model = new StationDetailsVM
                        {
                            Name = chosenStation.Name,
                            IdentificationCode = chosenStation.IdentificationCode,
                            Voivodeship = chosenStation.Voivodeship,
                            City = chosenStation.City,
                            Street = chosenStation.Street,
                            HouseNumber = chosenStation.HouseNumber,
                            ApartmentNumber = chosenStation.ApartmentNumber,
                            Photo = chosenStation.Photo,
                            Diagnosticians = chosenStation.Diagnosticians,
                            Owner = chosenStation.Owner
                        };
            }

            return View(model);
        }

        [Authorize(Roles = RolesEn.Admin)]
        public ActionResult Delete(string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var stationToDelete = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (stationToDelete == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Wystąpił błąd podczas usuwania SKP."});
                }

                dbContext.Stations.Remove(stationToDelete);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Poprawnie usunięto SKP."});
        }

        [Authorize(Roles = RolesEn.Admin + "," + RolesEn.Owner)]
        public ActionResult Edit(string identificationCode)
        {
            EditStationDetailsVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var stationToEdit = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (stationToEdit == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Wystąpił błąd podczas uzyskiwania SKP do edycji."});
                }

                model = new EditStationDetailsVM
                        {
                            Id = stationToEdit.Id,
                            Name = stationToEdit.Name,
                            IdentificationCode = stationToEdit.IdentificationCode,
                            Voivodeship = stationToEdit.Voivodeship,
                            City = stationToEdit.City,
                            Street = stationToEdit.Street,
                            HouseNumber = stationToEdit.HouseNumber,
                            ApartmentNumber = stationToEdit.ApartmentNumber,
                            Photo = stationToEdit.Photo
                        };
            }

            return View(model);
        }

        [Authorize(Roles = RolesEn.Admin + "," + RolesEn.Owner)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditStationDetailsVM model, HttpPostedFileBase image = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!EntityManager.IsIdentificationCodeUnique(model.Id, model.IdentificationCode))
            {
                ModelState.AddModelError(nameof(model.IdentificationCode), "Taki kod rozpoznawczy SKP już istnieje.");
                return View(model);
            }

            if (image != null)
            {
                var fileStream = image.InputStream;
                var binaryReader = new BinaryReader(fileStream);
                model.Photo = binaryReader.ReadBytes((int) fileStream.Length);
            }

            EntityManager.UpdateStationDetails(model);

            return RedirectToAction("Details", new {message = "Poprawnie zaktualizowano dane o SKP!", identificationCode = model.IdentificationCode});
        }

        [Authorize(Roles = RolesEn.Admin)]
        [HttpGet]
        public ActionResult Create(Guid requestId = new Guid(), string requestType = "")
        {
            StationDetailsVM model;

            if (requestId != Guid.Empty && !string.IsNullOrEmpty(requestType))
            {
                using (var dbContext = ApplicationDbContext.Create())
                {
                    switch (requestType)
                    {
                        case "stationRequest":
                        {
                            var stationRequest = dbContext.StationRequests.FirstOrDefault(request => request.Id == requestId);
                            model = new StationDetailsVM
                                    {
                                        Name = stationRequest?.Name,
                                        IdentificationCode = stationRequest?.IdentificationCode,
                                        Voivodeship = stationRequest?.Voivodeship,
                                        City = stationRequest?.City,
                                        Street = stationRequest?.Street,
                                        HouseNumber = stationRequest?.HouseNumber ?? 0,
                                        ApartmentNumber = stationRequest?.ApartmentNumber,
                                        Photo = stationRequest?.Photo
                                    };

                            dbContext.Dispose();
                            return View(model);
                        }
                        case "ownerStationRequest":
                        {
                            var ownerStationRequest = dbContext.OwnerStationRequests.Include(request => request.Owner).FirstOrDefault(request => request.Id == requestId);
                            model = new StationDetailsVM
                                    {
                                        Name = ownerStationRequest?.Name,
                                        IdentificationCode = ownerStationRequest?.IdentificationCode,
                                        Voivodeship = ownerStationRequest?.Voivodeship,
                                        City = ownerStationRequest?.City,
                                        Street = ownerStationRequest?.Street,
                                        HouseNumber = ownerStationRequest?.HouseNumber ?? 0,
                                        ApartmentNumber = ownerStationRequest?.ApartmentNumber,
                                        Photo = ownerStationRequest?.Photo
                                    };

                            dbContext.Dispose();
                            return View(model);
                        }
                    }
                }
            }

            model = new StationDetailsVM();

            return View(model);
        }

        [Authorize(Roles = RolesEn.Admin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StationDetailsVM model, HttpPostedFileBase image = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!EntityManager.IsIdentificationCodeUnique(model.IdentificationCode))
            {
                ModelState.AddModelError(nameof(model.IdentificationCode), "Taki kod rozpoznawczy SKP już istnieje.");
                return View(model);
            }

            if (image != null)
            {
                var fileStream = image.InputStream;
                var binaryReader = new BinaryReader(fileStream);
                model.Photo = binaryReader.ReadBytes((int) fileStream.Length);
            }

            var station = EntityManager.CreateStation(model.Name, model.IdentificationCode, model.Voivodeship, model.City, model.Street, model.HouseNumber, model.ApartmentNumber, model.Photo);

            using (var dbContext = ApplicationDbContext.Create())
            {
                dbContext.Stations.Add(station);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Poprawnie dodano SKP."});
        }

        [Authorize(Roles = RolesEn.Customer + "," + RolesEn.Diagnostician)]
        public ActionResult CreateStationRequest()
        {
            var model = new StationRequestVM();

            return View(model);
        }

        [Authorize(Roles = RolesEn.Customer + "," + RolesEn.Diagnostician)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateStationRequest(StationRequestVM model, HttpPostedFileBase image = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!EntityManager.IsStationRequestUseful(model))
            {
                ModelState.AddModelError("", "Niestety wysłany przez Ciebie formularz zawiera za mało danych, by mógł być dla nas przydatny. Spróbuj uzupełnić więcej pól, by wysłać wniosek.");
                return View(model);
            }

            if (!EntityManager.IsIdentificationCodeUnique(model.IdentificationCode))
            {
                ModelState.AddModelError(nameof(model.IdentificationCode), "Taki kod rozpoznawczy SKP już istnieje.");
                return View(model);
            }

            if (image != null)
            {
                var fileStream = image.InputStream;
                var binaryReader = new BinaryReader(fileStream);
                model.Photo = binaryReader.ReadBytes((int) fileStream.Length);
            }
            else
            {
                model.Photo = EntityManager.GetDefaultPhoto();
            }

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Users.FirstOrDefault(appUser => appUser.Id == userId);

                var request = new StationRequest
                              {
                                  KnownFrom = model.KnownFrom,
                                  Name = model.Name,
                                  IdentificationCode = model.IdentificationCode,
                                  Voivodeship = model.Voivodeship,
                                  City = model.City,
                                  Street = model.Street,
                                  HouseNumber = model.HouseNumber,
                                  ApartmentNumber = model.ApartmentNumber,
                                  OtherInfo = model.OtherInfo,
                                  Photo = model.Photo,
                                  User = user
                              };

                dbContext.StationRequests.Add(request);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Formularz został przesłany poprawnie."});
        }

        [Authorize(Roles = RolesEn.Owner)]
        public ActionResult CreateOwnerStationRequest()
        {
            var model = new OwnerStationRequestVM();

            return View(model);
        }

        [Authorize(Roles = RolesEn.Owner)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOwnerStationRequest(OwnerStationRequestVM model, HttpPostedFileBase certificate = null, HttpPostedFileBase image = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (certificate == null)
            {
                ModelState.AddModelError(nameof(model.Certificate), "Dokument potwierdzający istnienie SKP jest wymagany!");
                return View(model);
            }

            if (!EntityManager.IsIdentificationCodeUnique(model.IdentificationCode))
            {
                ModelState.AddModelError(nameof(model.IdentificationCode), "Taki kod rozpoznawczy SKP już istnieje.");
                return View(model);
            }

            var fileStream = certificate.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            model.Certificate = binaryReader.ReadBytes((int) fileStream.Length);

            if (image != null)
            {
                fileStream = image.InputStream;
                binaryReader = new BinaryReader(fileStream);
                model.Photo = binaryReader.ReadBytes((int) fileStream.Length);
            }
            else
            {
                model.Photo = EntityManager.GetDefaultPhoto();
            }

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Owners.FirstOrDefault(owner => owner.Id == userId);

                var request = new OwnerStationRequest
                              {
                                  Name = model.Name,
                                  IdentificationCode = model.IdentificationCode,
                                  Voivodeship = model.Voivodeship,
                                  City = model.City,
                                  Street = model.Street,
                                  HouseNumber = model.HouseNumber,
                                  ApartmentNumber = model.ApartmentNumber,
                                  Photo = model.Photo,
                                  Certificate = model.Certificate,
                                  Owner = user
                              };

                dbContext.OwnerStationRequests.Add(request);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Formularz został przesłany poprawnie."});
        }

        [Authorize(Roles = RolesEn.Owner)]
        public ActionResult CreateOwnershipRequest()
        {
            var model = new OwnershipRequestVM();

            return View(model);
        }

        [Authorize(Roles = RolesEn.Owner)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOwnershipRequest(OwnershipRequestVM model, HttpPostedFileBase certificate = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (certificate == null)
            {
                ModelState.AddModelError(nameof(model.Certificate), "Dokument potwierdzający istnienie SKP jest wymagany!");
                return View(model);
            }

            if (EntityManager.IsIdentificationCodeUnique(model.IdentificationCode))
            {
                ModelState.AddModelError(nameof(model.IdentificationCode), "Nie znaleziono SKP o takim kodzie rozpoznawczym.");
                return View(model);
            }

            var fileStream = certificate.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            model.Certificate = binaryReader.ReadBytes((int) fileStream.Length);

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Owners.FirstOrDefault(owner => owner.Id == userId);

                var request = new OwnershipRequest
                              {
                                  IdentificationCode = model.IdentificationCode,
                                  Certificate = model.Certificate,
                                  OtherInfo = model.OtherInfo,
                                  Owner = user
                              };

                dbContext.OwnershipRequests.Add(request);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Formularz został przesłany poprawnie."});
        }

        [Authorize(Roles = RolesEn.Customer)]
        public ActionResult AddToFavourites(string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Customers.FirstOrDefault(customer => customer.Id == userId);
                var stationToAdd = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (user == null || stationToAdd == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Dodawanie SKP do ulubionych nie powiodło się."});
                }

                user.FavouriteStations.Add(stationToAdd);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Stations", "Customer", new {message = "Pomyślnie dodano SKP do ulubionych."});
        }

        [Authorize(Roles = RolesEn.Diagnostician)]
        public ActionResult AddToWorkplaces(string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Diagnosticians.FirstOrDefault(diagnostician => diagnostician.Id == userId);
                var stationToAdd = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (user == null || stationToAdd == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Dodawanie SKP do miejsc pracy nie powiodło się."});
                }

                user.Workplaces.Add(stationToAdd);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Stations", "Diagnostician", new {message = "Pomyślnie dodano SKP do miejsc pracy."});
        }

        [Authorize(Roles = RolesEn.Customer)]
        public ActionResult RemoveFromFavourites(string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Customers.FirstOrDefault(customer => customer.Id == userId);
                var stationToRemove = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (user == null || stationToRemove == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Usuwanie SKP z ulubionych nie powiodło się."});
                }

                user.FavouriteStations.Remove(stationToRemove);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Stations", "Customer", new {message = "Pomyślnie usunięto SKP z ulubionych."});
        }

        [Authorize(Roles = RolesEn.Diagnostician)]
        public ActionResult RemoveFromWorkplaces(string identificationCode)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.Diagnosticians.FirstOrDefault(diagnostician => diagnostician.Id == userId);
                var stationToRemove = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == identificationCode);

                if (user == null || stationToRemove == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Usuwanie SKP z miejsc pracy nie powiodło się."});
                }

                user.Workplaces.Remove(stationToRemove);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Stations", "Diagnostician", new {message = "Pomyślnie usunięto SKP z miejsc pracy."});
        }

        public ActionResult Map()
        {
            List<StationDetailsVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.Stations
                                 .Select(station => new StationDetailsVM
                                                    {
                                                        Name = station.Name,
                                                        IdentificationCode = station.IdentificationCode,
                                                        Voivodeship = station.Voivodeship,
                                                        City = station.City,
                                                        Street = station.Street,
                                                        HouseNumber = station.HouseNumber,
                                                        ApartmentNumber = station.ApartmentNumber,
                                                        Photo = station.Photo,
                                                        Diagnosticians = station.Diagnosticians,
                                                        Owner = station.Owner
                                                    })
                                 .ToList();
            }

            return View(model);
        }

        [Authorize(Roles = RolesEn.Admin)]
        public ActionResult StationRequest(Guid id)
        {
            StationRequest model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.StationRequests.Include(request => request.User).FirstOrDefault(request => request.Id == id);

                if (model == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("StationRequestList", "Admin", new {message = "Wystąpił błąd podczas wczytywania wniosku"});
                }
            }

            return View(model);
        }

        [Authorize(Roles = RolesEn.Admin)]
        public ActionResult OwnerStationRequest(Guid id)
        {
            OwnerStationRequest model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.OwnerStationRequests.Include(request => request.Owner).FirstOrDefault(request => request.Id == id);

                if (model == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("OwnerStationRequestList", "Admin", new {message = "Wystąpił błąd podczas wczytywania wniosku"});
                }
            }

            return View(model);
        }

        [Authorize(Roles = RolesEn.Admin)]
        public ActionResult OwnershipRequest(Guid id)
        {
            OwnershipRequest model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.OwnershipRequests.Include(request => request.Owner).FirstOrDefault(request => request.Id == id);

                if (model == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("OwnershipRequestList", "Admin", new {message = "Wystąpił błąd podczas wczytywania wniosku"});
                }
            }

            return View(model);
        }
    }
}
