﻿namespace SKPOnline.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;
    using Database;
    using Models;
    using Models.ViewModels;

    [Authorize(Roles = RolesEn.Admin)]
    public class AdminController : Controller
    {
        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            RequestsCounterVM model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = new RequestsCounterVM
                        {
                            NumberOfStationRequests = dbContext.StationRequests.Count(),
                            NumberOfOnwerStationRequests = dbContext.OwnerStationRequests.Count(),
                            NumberOfOwnershipRequests = dbContext.OwnershipRequests.Count()
                        };
            }

            return View(model);
        }

        public ActionResult StationRequestList(string message)
        {
            ViewBag.StatusMessage = message;

            List<StationRequestVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.StationRequests
                                 .Select(request => new StationRequestVM
                                                    {
                                                        Id = request.Id,
                                                        KnownFrom = request.KnownFrom,
                                                        Name = request.Name,
                                                        IdentificationCode = request.IdentificationCode,
                                                        Voivodeship = request.Voivodeship,
                                                        City = request.City,
                                                        Street = request.Street,
                                                        HouseNumber = request.HouseNumber,
                                                        ApartmentNumber = request.ApartmentNumber,
                                                        OtherInfo = request.OtherInfo,
                                                        Photo = request.Photo
                                                    })
                                 .ToList();
            }

            return View(model);
        }

        public ActionResult OwnerStationRequestList(string message)
        {
            ViewBag.StatusMessage = message;

            List<OwnerStationRequestVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.OwnerStationRequests
                                 .Select(request => new OwnerStationRequestVM
                                                    {
                                                        Id = request.Id,
                                                        Name = request.Name,
                                                        IdentificationCode = request.IdentificationCode,
                                                        Voivodeship = request.Voivodeship,
                                                        City = request.City,
                                                        Street = request.Street,
                                                        HouseNumber = request.HouseNumber,
                                                        ApartmentNumber = request.ApartmentNumber,
                                                        Photo = request.Photo,
                                                        Certificate = request.Certificate
                                                    })
                                 .ToList();
            }

            return View(model);
        }

        public ActionResult OwnershipRequestList(string message)
        {
            ViewBag.StatusMessage = message;

            List<OwnershipRequestVM> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.OwnershipRequests
                                 .Select(request => new OwnershipRequestVM
                                                    {
                                                        Id = request.Id,
                                                        IdentificationCode = request.IdentificationCode,
                                                        Certificate = request.Certificate,
                                                        OtherInfo = request.OtherInfo
                                                    })
                                 .ToList();
            }

            return View(model);
        }

        public ActionResult DeleteOwnershipRequest(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var requestToDelete = dbContext.OwnershipRequests.FirstOrDefault(request => request.Id == id);

                if (requestToDelete == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("OwnershipRequestList", new {message = "Wystąpił błąd podczas usuwania wniosku."});
                }

                dbContext.OwnershipRequests.Remove(requestToDelete);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Poprawnie usunięto wniosek."});
        }

        public ActionResult DeleteOwnerStationRequest(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var requestToDelete = dbContext.OwnerStationRequests.FirstOrDefault(request => request.Id == id);

                if (requestToDelete == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("OwnerStationRequestList", new {message = "Wystąpił błąd podczas usuwania wniosku."});
                }

                dbContext.OwnerStationRequests.Remove(requestToDelete);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Poprawnie usunięto wniosek."});
        }

        public ActionResult DeleteStationRequest(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var requestToDelete = dbContext.StationRequests.FirstOrDefault(request => request.Id == id);

                if (requestToDelete == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("StationRequestList", new {message = "Wystąpił błąd podczas usuwania wniosku."});
                }

                dbContext.StationRequests.Remove(requestToDelete);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Poprawnie usunięto wniosek."});
        }

        public ActionResult RedirectToAddStationFromRequest(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var stationRequest = dbContext.StationRequests.FirstOrDefault(request => request.Id == id);

                if (stationRequest != null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Create", "Station", new {requestId = stationRequest.Id, requestType = nameof(stationRequest)});
                }

                var ownerStationRequest = dbContext.OwnerStationRequests.Include(request => request.Owner).FirstOrDefault(request => request.Id == id);

                if (ownerStationRequest == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Nie udało się przesłać wniosku do formularza."});
                }

                dbContext.Dispose();
                return RedirectToAction("Create", "Station", new {requestId = ownerStationRequest.Id, requestType = nameof(ownerStationRequest)});
            }
        }

        public ActionResult RedirectToGrantOwnershipFromRequest(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var ownershipRequest = dbContext.OwnershipRequests.FirstOrDefault(request => request.Id == id);

                if (ownershipRequest == null)
                {
                    var ownerStationRequest = dbContext.OwnerStationRequests.FirstOrDefault(request => request.Id == id);

                    if (ownerStationRequest == null)
                    {
                        dbContext.Dispose();
                        return RedirectToAction("Index", new {message = "Nie udało się przesłać wniosku do formularza."});
                    }

                    dbContext.Dispose();
                    return RedirectToAction("GrantOwnership", new {requestId = ownerStationRequest.Id, requestType = nameof(ownerStationRequest), identificationCode = ""});
                }

                dbContext.Dispose();
                return RedirectToAction("GrantOwnership", new {requestId = ownershipRequest.Id, requestType = nameof(ownershipRequest), identificationCode = ""});
            }
        }

        public ActionResult GrantOwnership(Guid requestId = new Guid(), string requestType = "", string identificationCode = "")
        {
            GrantOwnershipVM model;

            if (requestId != Guid.Empty && !string.IsNullOrEmpty(requestType))
            {
                using (var dbContext = ApplicationDbContext.Create())
                {
                    switch (requestType)
                    {
                        case "ownershipRequest":
                        {
                            var ownershipRequest = dbContext.OwnershipRequests.FirstOrDefault(request => request.Id == requestId);
                            model = new GrantOwnershipVM
                                    {
                                        IdentificationCode = ownershipRequest?.IdentificationCode,
                                        OwnerId = ownershipRequest?.Owner.Id
                                    };

                            dbContext.Dispose();
                            return View(model);
                        }
                        case "ownerStationRequest":
                        {
                            var ownerStationRequest = dbContext.OwnerStationRequests.FirstOrDefault(request => request.Id == requestId);
                            model = new GrantOwnershipVM
                                    {
                                        IdentificationCode = ownerStationRequest?.IdentificationCode,
                                        OwnerId = ownerStationRequest?.Owner.Id
                                    };

                            dbContext.Dispose();
                            return View(model);
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(identificationCode))
            {
                model = new GrantOwnershipVM
                        {
                            IdentificationCode = identificationCode,
                        };

                return View(model);
            }

            model = new GrantOwnershipVM();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("GrantOwnership")]
        public ActionResult GrantOwnershipPost(GrantOwnershipVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var dbContext = ApplicationDbContext.Create())
            {
                var requestedStation = dbContext.Stations.FirstOrDefault(station => station.IdentificationCode == model.IdentificationCode);

                if (requestedStation == null)
                {
                    ModelState.AddModelError(nameof(model.IdentificationCode), "Stacja o takim kodzie rozpoznawczym nie istnieje.");
                    return View(model);
                }

                var existingOwner = requestedStation.Owner;

                if (existingOwner != null)
                {
                    ModelState.AddModelError(nameof(model.IdentificationCode), "Ta stacja już posiada właściciela.");
                    return View(model);
                }

                var ownerToAdd = dbContext.Owners.FirstOrDefault(owner => owner.Id == model.OwnerId);

                if (ownerToAdd == null)
                {
                    ModelState.AddModelError(nameof(model.IdentificationCode), "Właściciel o takim ID nie istnieje.");
                    return View(model);
                }

                requestedStation.Owner = ownerToAdd;
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Poprawnie dodano właściciela SKP."});
        }
    }
}
