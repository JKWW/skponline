﻿using Microsoft.Owin;
using SKPOnline;

[assembly: OwinStartupAttribute(typeof(Startup))]

namespace SKPOnline
{
    using Owin;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
