﻿namespace SKPOnline.Database
{
    using System;
    using System.Data.Entity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDisposable
    {
        public ApplicationDbContext() : base("SKPOnline", false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Owner> Owners { get; set; }
        public DbSet<Diagnostician> Diagnosticians { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<StationRequest> StationRequests { get; set; }
        public DbSet<OwnerStationRequest> OwnerStationRequests { get; set; }
        public DbSet<OwnershipRequest> OwnershipRequests { get; set; }
        public void Dispose()
        {
            base.Dispose();
        }
    }
}
