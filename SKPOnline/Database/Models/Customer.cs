﻿namespace SKPOnline.Database.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Customer
    {
        [ForeignKey("ApplicationUser")]
        [Required(ErrorMessage = "Id profilu klienta jest wymagane!")]
        public string Id { get; set; }

        [Display(Name = "Nazwa użytkownika")]
        [Index(IsUnique = true)]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Nazwa użytkownika może mieć od {2} do {1} znaków.")]
        public string NickName { get; set; }

        [Display(Name = "Płeć")]
        public string Gender { get; set; }

        [Display(Name = "Imię")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Imię może mieć od {2} do {1} znaków.")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Nazwisko może mieć od {2} do {1} znaków.")]
        public string Surname { get; set; }

        [Display(Name = "Data urodzenia")]
        [DataType(DataType.Date, ErrorMessage = "Niewłaściwy typ danych.")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "Numer telefonu")]
        [Phone(ErrorMessage = "Wprowadzony numer telefonu ma niepoprawny format.")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Województwo")]
        public string Voivodeship { get; set; }

        [Display(Name = "Miejscowość")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości może mieć od {2} do {1} znaków.")]
        public string City { get; set; }

        [Display(Name = "Ulica")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Nazwa ulicy może mieć od {2} do {1} znaków.")]
        public string Street { get; set; }

        [Display(Name = "Numer domu")]
        [Range(1, 1000, ErrorMessage = "Numer domu może być w przedziale od {1} do {2}")]
        public short? HouseNumber { get; set; }

        [Display(Name = "Numer mieszkania")]
        [Range(1, 1000, ErrorMessage = "Numer mieszkania może być w przedziale od {1} do {2}")]
        public short? ApartmentNumber { get; set; }

        [Display(Name = "Zdjęcie profilowe")]
        public byte[] Avatar { get; set; }

        [Display(Name = "Twoje ulubione stacje")]
        public virtual List<Station> FavouriteStations { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
