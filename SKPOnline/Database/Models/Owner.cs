﻿namespace SKPOnline.Database.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Owner
    {
        [ForeignKey("ApplicationUser")]
        [Required(ErrorMessage = "Id profilu właściciela jest wymagane!")]
        public string Id { get; set; }

        [Display(Name = "Nazwa firmy")]
        [Index(IsUnique = true)]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Nazwa firmy może mieć od {2} do {1} znaków.")]
        public string CompanyName { get; set; }

        [Display(Name = "NIP")]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Niepoprawny numer NIP.")]
        public string Nip { get; set; }

        [Display(Name = "Numer telefonu")]
        [Phone(ErrorMessage = "Wprowadzony numer telefonu ma niepoprawny format.")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Adres e-mail firmy")]
        [EmailAddress(ErrorMessage = "Wprowadzony adres e-mail ma niepoprawny format.")]
        public string CompanyEmail { get; set; }

        [Display(Name = "Województwo")]
        public string Voivodeship { get; set; }

        [Display(Name = "Miejscowość")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości może mieć od {2} do {1} znaków.")]
        public string City { get; set; }

        [Display(Name = "Ulica")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "Nazwa ulicy może mieć od {2} do {1} znaków.")]
        public string Street { get; set; }

        [Display(Name = "Numer posesji")]
        [Range(1, 1000, ErrorMessage = "Numer posesji może być w przedziale od {1} do {2}")]
        public short? HouseNumber { get; set; }

        [Display(Name = "Numer lokalu")]
        [Range(1, 1000, ErrorMessage = "Numer lokalu może być w przedziale od {1} do {2}")]
        public short? ApartmentNumber { get; set; }

        [Display(Name = "Logo firmy")]
        public byte[] Logo { get; set; }

        [Display(Name = "Moje stacje")]
        public virtual List<Station> MyStations { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
