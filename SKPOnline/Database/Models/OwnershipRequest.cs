﻿namespace SKPOnline.Database.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class OwnershipRequest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "Id wniosku jest wymagane!")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Kod rozpoznawczy SKP jest wymagany")]
        [Display(Name = "Kod rozpoznawczy SKP")]
        [StringLength(9, MinimumLength = 6, ErrorMessage = "Kod rozpoznawczy stacji może mieć od {2} do {1} znaków.")]
        [RegularExpression(@"^[A-Z]{2,3}\/[0-9]{3}(\/[P])?$", ErrorMessage = "Kod rozpoznawczy stacji jest nieprawidłowy.")]
        public string IdentificationCode { get; set; }

        [Display(Name = "Certyfikat potwierdzający istnienie SKP")]
        [Required(ErrorMessage = "Dokument potwierdzający istnienie SKP jest wymagany!")]
        public byte[] Certificate { get; set; }

        [Display(Name = "Pozostałe informacje potwierdzające prawa do SKP")]
        [StringLength(200, ErrorMessage = "Pozostałe informacje mogą mieć maksymalnie {1} znaków")]
        public string OtherInfo { get; set; }

        [Required(ErrorMessage = "Właściciel zgłaszjący prawo do SKP jest wymagany!")]
        [Display(Name = "Właściciel zgłaszający prawo do SKP")]
        public virtual Owner Owner { get; set; }
    }
}
