﻿<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SKPOnline</title>

        <link rel="stylesheet" href="/Content/bootstrap.css">
        <link rel="stylesheet" href="/Content/Site.css">

        <script src="/Scripts/jquery-3.6.0.js"></script>
        <script src="/Scripts/bootstrap.js"></script>
        <script src="/Scripts/modernizr-2.8.3.js"></script>
    </head>

    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"></button>
                    <a class="navbar-brand" href="/">Strona główna</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/Home/About">Informacje</a></li>
                        <li><a href="/Home/Contact">Kontakt</a></li>
                        <li><a href="/Station">Lista SKP</a></li>
                        <li><a href="/Station/Map">Mapa SKP</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container body-content">
            <h2 class="text-danger">W trakcie działania aplikacji wystąpił nieoczekiwany błąd.</h2>
            <h3 class="text-danger">Spróbuj ponownie wykonać operację, a w przypadku niepowodzenia skontaktuj się z administratorem serwisu.</h3>

            <hr/>

            <a href="/">Powrót do strony głównej</a>

            <hr/>

            <footer>
                <p>&copy; 2022 - SKPOnline - Created by Wójcik Jakub</p>
            </footer>
        </div>
    </body>
</html>